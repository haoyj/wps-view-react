import React from "react"
import {Pagination,ConfigProvider} from 'antd';
import zhCN from 'antd/es/locale/zh_CN';
import { scrollTo } from '../utils/scroll-to'

export default class MyPagination extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            pageSizes:["10","20","50","100"],
        };
    }

    render() {
        return <div  className={'pagination'}>
                <ConfigProvider locale={zhCN}>
                    <Pagination
                        showTotal={total => `共 ${total} 条`}
                        total={this.props.total}
                        showSizeChanger
                        size="small"
                        pageSize={this.props.size}
                        currentPage={this.props.page}
                        pageSizeOptions={this.state.pageSizes}
                        onShowSizeChange={this.onShowSizeChange.bind(this)}
                        onChange={this.onChange.bind(this)}
                    />
                </ConfigProvider>
            </div>
    }

    onShowSizeChange = (page, pageSize)=>{
        const par = {
            page:page === 0 ? 1 :page,
            size:pageSize
        };
        this.props.pageChange(par);
        scrollTo(0,800,()=>{})
    };

    onChange = (current, size) => {
        const par = {
            page:current,
            size:size
        };
        this.props.pageChange(par);
        scrollTo(0,800,()=>{})
    }

}
